# pybounce

creating a theoretical "bounce" effect for zooming a phone screen across a 
"map" over an arc (in `x` and `zoom`), where x can be any distance within 20
miles(?). the desired effect will have a smooth motion along the "screen space" 
pixels as this screen moves - like a camera facing down - along an arc to be 
calculated. every pixel in `screen_x, screen_y` should experience relatively
constant motion, the derivative of their position should be an integer(?!)